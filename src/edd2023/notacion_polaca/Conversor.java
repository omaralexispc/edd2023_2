/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.notacion_polaca;

import edd2023.project.listas.stack.Stack;

/**
 *
 * @author Hp
 */
public class Conversor {
    
    public Conversor(){
        
    }
    
    public String convertir(String normal){
        //se asumen todos los caracteres como validos matematicamente
        Stack<Character> stack = new Stack();
        String convertida = "";
        normal = normal.toUpperCase();
        for (Character c:normal.toCharArray()){
            //caso variable/numero
            //todas las letras mayusculas
            if(c>=48&&c<=90){
                convertida+=c;
            }
            //caso operador
            else{
                //System.out.println("Stack:"+stack.peek());
                //casos corchetes
                if (c==')'){
                    while (stack.peek()!='('&&!stack.isEmpty() ){
                       
                        convertida+=stack.pop();
                        
                        
                    }
                    if (stack.peek()=='(') stack.pop();
                }
                else if (c=='(') stack.push(c);
                else{
                    //si jerarquia entrante es menor o igual a jerarquia de cabeza de pila
                    if (asignarJerarquia(c)<=asignarJerarquia(stack.peek()) ) {
                        convertida+=stack.pop();
                    }
                    stack.push(c);
                }
                
                
                
            }
        }
        if (!stack.isEmpty()){
            while (!stack.isEmpty()){
                convertida+=stack.pop();
            }
        }
        
        return convertida;
    }
    
    public int asignarJerarquia(Character c){
        int jerarquia = -1;
        if (c==null) return jerarquia;
        if (c=='^') jerarquia = 3;
        if (c=='*'||c=='/') jerarquia = 2;
        if (c=='+'||c=='-') jerarquia = 1;
        return jerarquia;
    }
    
    
   
}
