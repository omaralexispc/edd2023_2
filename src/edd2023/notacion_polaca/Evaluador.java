/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.notacion_polaca;

import edd2023.project.listas.stack.Stack;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Hp
 */
public class Evaluador {

    public Evaluador() {

    }

    public float resolverMultiplesDigitos(String expresion) {
        float resultado = 0;
        //la expresion se asume infija
        //mapear numeros con variables en tabla hash
        HashMap<Character, Integer> tablaNumerica = crearTablaNumerica(expresion);
        System.out.println("Tabla numerica:" + tablaNumerica);
        System.out.println("Expresion original:" + expresion);

        //Reemplazar numeros por variables en la expresion
        expresion = reemplazarNumerosVariables(tablaNumerica, expresion);
        System.out.println("Nueva expresion:" + expresion);
        
        //convertir expresion alfabetica infija a posfija
        Conversor con = new Conversor();
        expresion = con.convertir(expresion);
        System.out.println("Expresion convertida a notacion posfija:" + expresion);

        //finalmente, resolver con tabla hash a la mano
        Stack<Float> stack = new Stack();

        char[] chars = expresion.toCharArray();
        for (char c : chars) {
            System.out.println("char:" + c);
            if (c >= 65 && c <= 90) {
                stack.push((float) tablaNumerica.get(c));
            } else {
                float subresultado = operar(c, stack.pop(), stack.pop());
                resultado = subresultado;
                stack.push(subresultado);
            }

            System.out.println("resultado:" + resultado);
        }

        return resultado;
    }
    
    
    public String reemplazarNumerosVariables(HashMap<Character, Integer> tablaNumerica,String expresion){
        for (Map.Entry<Character, Integer> entry : tablaNumerica.entrySet()) {
            expresion = expresion.replaceAll("\\b" + entry.getValue() + "\\b", entry.getKey() + "");
        }
        return expresion;
    }
    
    public HashMap<Character, Integer> crearTablaNumerica(String expresion) {
    HashMap<Character, Integer> tablaNumerica = new HashMap();
        String numero = "";
        for (char c : expresion.toCharArray()) {
            if (c >= 48 && c <= 57) {
                numero += c + "";
            } else {
                //en caso de que hallan parentesis
                if (!numero.isBlank()) {
                    int numeroValor = Integer.parseInt(numero);
                    //condicional para no admitir numeros repetidos y tomar variables
                    if (!tablaNumerica.containsValue(numeroValor)) {
                        tablaNumerica.put((char) (65 + tablaNumerica.size()), numeroValor);
                    }
                    numero = "";
                }
            }
        }
        //para incluir al ultimo numero
        if (!numero.isBlank()) {
            if (!tablaNumerica.containsValue(Integer.parseInt(numero))) {
                tablaNumerica.put((char) (65 + tablaNumerica.size()), Integer.parseInt(numero));
            }
        }
        return tablaNumerica;
    }

    public float resolverUnDigito(String expresion) {
        //la expresion se asume infija

        System.out.println("Expresion original:" + expresion);
        Conversor con = new Conversor();
        expresion = con.convertir(expresion);
        System.out.println("Expresion convertida a notacion posfija:" + expresion);

        float resultado = 0;
        Stack<Float> stack = new Stack();

        char[] chars = expresion.toCharArray();
        for (char c : chars) {
            System.out.println("char:" + c);
            if (c >= 48 && c <= 57) {
                stack.push(Float.parseFloat(c + ""));
            } else {
                float subresultado = operar(c, stack.pop(), stack.pop());
                resultado = subresultado;
                stack.push(subresultado);
            }

            System.out.println("resultado:" + resultado);
        }
        return resultado;
    }

    public float operar(char operador, float segundo, float primero) {
        float resultado = 0;
        if (operador == '+') {
            resultado = primero + segundo;
        }
        if (operador == '-') {
            resultado = primero - segundo;
        }
        if (operador == '/') {
            if (segundo == 0) {
                throw new RuntimeException("Division por cero");
            }
            resultado = primero / segundo;
        }
        if (operador == '*') {
            //System.out.println("Primero:"+primero+" segundo:"+segundo);
            resultado = primero * segundo;
        }
        if (operador == '^') {
            resultado = (float) Math.pow(primero, segundo);
        }
        return resultado;
    }

}
