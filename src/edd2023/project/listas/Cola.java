/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

/**
 *
 * @author Estudiante
 */
public class Cola<E extends Comparable> {
    private LinkedList<E> lista;
    
    public Cola(){
        lista = new LinkedList();
    }
    
    public void encolar (E val){
        lista.add(val);
    }
    
    public E desencolar (){
       
        return lista.deleteAt(0);
    }
    
    public E primero(){
        return lista.getAt(0);
    }
    
    public E ultimo(){
        return lista.getAt(lista.getSize()-1);
    }
    
    
}
