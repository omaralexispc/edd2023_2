package edd2023.project.listas;

public class DoubleNode<E extends Comparable> {

	private E value;
	private DoubleNode<E> prev;
	private DoubleNode<E> next;
	
	public DoubleNode() {
		super();
	}

	public DoubleNode(E value) {
		super();
		this.value = value;
	}

	public DoubleNode(E value, DoubleNode<E> prev, DoubleNode<E> next) {
		super();
		this.value = value;
		this.prev = prev;
		this.next = next;
	}



	public E getVal() {
		return value;
	}



	public void setVal(E value) {
		this.value = value;
	}



	public DoubleNode<E> getPrev() {
		return prev;
	}



	public void setPrev(DoubleNode<E> prev) {
		this.prev = prev;
	}



	public DoubleNode<E> getNext() {
		return next;
	}



	public void setNext(DoubleNode<E> next) {
		this.next = next;
	}

	

}