/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

/**
 *
 * @author Hp
 */
public class DoubleLinkedList<E extends Comparable> {
    private DoubleNode head;
    private int size;
    public DoubleLinkedList(){
        
    }
    
    private DoubleNode getNode (int index){
        if (index>size-1) throw new RuntimeException("Indice mayor a tamaño de lista");
        DoubleNode p = head;
        for (int i = 0; i < index; i++) {
        p = p.getNext();    
        }
       return p; 
        
    }
    
    public E get(E val){
        DoubleNode<E> p = head;
        for (int i = 0; p!=null&&!p.getVal().equals(val); i++) {
        p = p.getNext();    
        }
       return p.getVal(); 
    }
    
    
    public void print(){
        DoubleNode pointer = head;
		System.out.println("Lista doble:");
		while (pointer!=null) {
			System.out.println(pointer.getVal()+" ->");
			pointer = pointer.getNext();
		}
    }
    public void add(E val) {
        DoubleNode n = new DoubleNode(val);
		if (head == null){
                    head = n;
                     size++;
                    return;
                }
                else{
                    DoubleNode p = head;
                    while (p.getNext()!=null){
                        p=p.getNext();
                    }
                    DoubleNode newNode = new DoubleNode<E> (val);
                    p.setNext(newNode);
                    newNode.setPrev(p);
                }
                size++;
		
	}
    
   void printGetNode(int index){
       DoubleNode p = getNode(index);
       System.out.println("GetNode:"+p.getVal());
   }
    
    
}
