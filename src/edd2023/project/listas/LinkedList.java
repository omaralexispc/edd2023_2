package edd2023.project.listas;

import java.util.Iterator;



public class LinkedList<E extends Comparable> {

	private Node<E> head;
	public int size;
	public LinkedList() {
		
	}
        
        public Iterator getIterator(){
            return new IteratorLS(head);
        }
        
	private Node<E> getHead(){
            return this.head;
        }
        
        public void sortDes(){
            //mayor a menor
         if (head==null) return;
         boolean haCambiado = false;
         
         for (Node<E> p = head; p!=null; p=p.getNext() ) {
             System.out.println("Nodo p:"+p.getVal());
             Node<E> q = p.getNext();
             
             while (q!=null) {                 
                 if (p.getVal().compareTo(q.getVal())<0){
                     E aux = p.getVal();
                     p.setVal(q.getVal());
                     q.setVal(aux);
                     haCambiado = true;
                 }
                 q=q.getNext();
             }
             
         }
         
         if (haCambiado) sortDes();
         
         }
    
    public void sortAs(){
        if (head==null) return;
         boolean haCambiado = false;
         
         for (Node<E> p = head; p!=null; p=p.getNext() ) {
             //System.out.println("Nodo p:"+p.getVal());
             Node<E> q = p.getNext();
             
             while (q!=null) {                 
                 if (p.getVal().compareTo(q.getVal())>0){
                     E aux = p.getVal();
                     p.setVal(q.getVal());
                     q.setVal(aux);
                     haCambiado = true;
                 }
                 q=q.getNext();
             }
             
         }
         
         if (haCambiado) sortAs();
    }
        public void join(LinkedList l){
            if (this.size==0) {
                head = l.getHead();
                return;
            }
            
            Node last = getNode(size-1);
            last.setNext(l.getHead() );
        }
        
	public void add(E val) {
		Node n = new Node(val);
		
		if (head!=null) {
			Node pointer = head;
			while (pointer.getNext()!=null) {
			pointer= pointer.getNext();
			}
			pointer.setNext(n);
		}
		else {
			head=n;
		}
                size++;
		
	}
	
	public void print() {
		Node pointer = head;
		System.out.println("Lista:");
		while (pointer!=null) {
			System.out.println(pointer.getVal()+" ->");
			pointer = pointer.getNext();
		}
	}
	
	public Node getNode (int index) {
            if (index>(size-1) ) return null;
            Node pointer = head;
		for (int i = 0; index!=0 && i < index && pointer!=null ; i++) {
			pointer = pointer.getNext();
		}
                return pointer;
	}

    public int getSize() {
       return this.size;
    }
    
    public void addOrdered(E val){
        Node n = new Node(val);
        Node p = head;
        
        //si la cabeza es nula, agrega de forma normal 
        if (p==null) {add(val); }
        else{
            //ciclo para encontrar el nodo mayor al value provisto. 
            //La logica es la misma que la de addBefore
            boolean encontrado = false;
            while (!encontrado&&p!=null){
                encontrado = (p.getVal().compareTo( val) )>=0;
                if (!encontrado) p = p.getNext();
            }
            System.out.println("encontrado:"+encontrado);
            
            if (encontrado){
                //si p termina en la cabeza, es decir si la cabeza es mayor a val
                // el nuevo nodo sera la cabeza de la lista
                if (p==head) {head=n; head.setNext(p);}
                else{
                    
                    //nodo q se ubicara detras de p, de tal forma que q->n->p
                    Node q = head;
                    while (q.getNext()!=p) q=q.getNext();
                    n.setNext(p);
                    q.setNext(n);
                }
            }
            else{
                //si el ciclo termino y no se ha encontrado un nodo, significa que no hay valuees mayores.
                //el nuevo nodo se agregara de forma normal
                //se resta uno a size para mantener el numero correcto
                add(val); size--;
            }
        }
        size++;
        
    }
	
	
	public void delete(E element) {
            
		// TODO Considerar los posibles casos
		// 1) Si el elemento a borrar está en la cabeza
		// 2) Si el elemento a borrar está en la mitad
		// 3) Si el elemento a borrar está en la cola
		
                if (element == null) return;
		//caso 1
                
                if (head.getVal().equals(element)){
                    
                    head = head.getNext();
                    size--;
                    return;
                }
                    
		// ---------------------------
		// Se considera el caso (2)
		// ---------------------------
		
		// Se pregunta si hay un elemento luego de la cabeza
		if(head.getNext()!=null) {
		
			Node<E> previous = head;
			
			while(previous.getNext()!=null) {
					
				Node<E> toFind = previous.getNext();
				
				if(!element.equals(toFind.getVal())){
					previous = toFind;
				}else {
					 
                                    Node<E> current = toFind.getNext();
					previous.setNext(current);
					size--;
				
					// ¿Qué pasa con la relación que queda del 
					// nodo a eliminar y el nodo que le sigue? 
				}
								
			}
		
		}	
		
		// ---------------------------
		// Se considera el caso (1)
		// ---------------------------
		
		
		// ---------------------------
		// Se considera el caso (3)
		// ---------------------------
		
	}
        
        public E deleteAt(int pos){
            
            if (isEmpty()) return null;
            if (pos>(size-1 )||pos<0) throw new RuntimeException("Indice invalido");
            if (pos==0) {
                E val = head.getVal();
                head = head.getNext();
                size--;
            
                return val;
            }
            else{
                Node<E> p = getNode(pos-1);
                Node<E> eliminar = p.getNext();
                p.setNext(eliminar.getNext());
                size--;
            
                return eliminar.getVal();
            }
            
            
           
            
        }
       
	
	public E getAt(int pos) {
		if (isEmpty()) return null;
		if (pos> (size-1)||pos<0 ) throw new RuntimeException("Posicion mayor al tamaño de la lista:"+size);
		Node<E> p = head;
		for (int i = 0;i<pos;i++){
				p = p.getNext();
				                
		}
		
		return p.getVal();
	}
	
	
	/**
	 * 
	 * @param element
	 * @param pos
	 */
	public void addAt(E element, int pos) {
                Node p = getNode(pos);
                Node n = new Node(element);
                if (p==head){
                    n.setNext(p);
                    head = n;
                    
                }
                else{
                    Node q = getNode(pos-1);
                    q.setNext(n);
                    n.setNext(p);
                }
                size++;
	}
        
        public void append (E val){
            addAt(val,0);
        }
	
	/**
	 * 
	 * @param pos
	 * @return
	 */
	/*public E remove(int pos) throws Exception{
	
		int[] arreglo = new int[3];
		arreglo[6] = 4;
		
		
		if(pos>=size)
			throw new Exception("La posición no puede ser mayor a "+size);
		
		return null;
	}*/
	
	/**
	 * 
	 */
	public void clear() {
		size = 0;
		head = null;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		
		if(head==null && size==0)
			return true;
		
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public E getFirst() throws Exception{
		if(!isEmpty())
			return head.getVal();
		
		throw new Exception("La lista está vacía");
		
	
	}
	
	/**
	 * 
	 * @param n1
	 * @param n2
	 */
	public void exchangeNodes(E valNodeToFind, E valNodeToInsert) {
		Node pointer = head;
		while (pointer!=null&&!pointer.getVal().equals(valNodeToFind) ){
                    pointer = pointer.getNext();
                }
                if (pointer!=null){
                    pointer.setVal(valNodeToInsert);
                }
                
	}
        
        public E deleteLast() {
            if (size==0) throw new RuntimeException ("Imposible, no hay elementos");
            if (size==1) {
                E val = (E) head.getVal();
                head = null;
                size--;
                return val;
                    }
            Node<E> p = getNode(size-2);
            E val = (E) p.getNext().getVal(); 
            p.setNext(null);
            size--;
            return val;
        }
        
        public E getLast(){
            Node<E> p = getNode(size-1);
           
            return (p==null)? null:(E) p.getVal();
        }
        
        public boolean contains (E val) {
        	boolean exists = false;
        	Node p = head;
        	while (!exists && p!=null) {
        		exists = p.getVal().equals(val);
        		p=p.getNext();
        	}
        	return exists;
        }
        
        public E get (E val){
            E returnable = null;
            Node p = head;
            while (p!=null){
                
            }
            return null;
        }
        
        
        

    
}
