/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

import java.util.Iterator;

/**
 *
 * @author Hp
 */
public class testIterator {
    public static void main(String[] args) {
        LinkedList l1 =  new LinkedList();
        l1.add(12);
        l1.add("lole");
        l1.add(Math.PI);
        Iterator i = l1.getIterator();
        while (i.hasNext()){
            System.out.println("Valor iterador:"+i.next());
        }
    }
    
}
