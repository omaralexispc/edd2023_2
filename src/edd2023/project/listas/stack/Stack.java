/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas.stack;

import edd2023.project.listas.LinkedList;

/**
 *
 * @author Hp
 */
public class Stack<E extends Comparable> {
    private LinkedList list;
    
    public Stack (){
        list = new LinkedList<E>();
    }
    
    public  void push(E element){
        list.add(element);
    }
    
    public E pop()  {
        return (E) list.deleteLast();
    }
    
    public E peek(){
        return (E) list.getLast();
    }
    
    public boolean isEmpty(){
        return list.isEmpty();
    }
    
}
