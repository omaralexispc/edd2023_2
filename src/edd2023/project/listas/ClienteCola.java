/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package edd2023.project.listas;

/**
 *
 * @author Estudiante
 */
public class ClienteCola {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Cola cola = new Cola();
        cola.encolar(10);
        cola.encolar(20);
        cola.encolar(30);
        cola.encolar(40);
        cola.encolar(50);
        cola.encolar(60);
        
        System.out.println("primero:"+cola.primero());
        System.out.println("ultimo:"+cola.ultimo());
        
        
        
        System.out.println("desocupar la cola\n\n");
       
       while (cola.primero()!=null){
           System.out.println("desencolar:"+cola.desencolar());
        
        System.out.println("primero:"+cola.primero());
        System.out.println("ultimo:"+cola.ultimo());
           System.out.println("\n");
       }
        
        
    }
    
}
