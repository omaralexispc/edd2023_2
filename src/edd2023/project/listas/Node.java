package edd2023.project.listas;

public class Node<E extends Comparable> {
	private E val;
	private Node next;
	
	public Node() {
		
	}
	
	public Node(E val) {
		this.val=val;
	}

	protected E getVal() {
		return val;
	}

	protected void setVal(E val) {
		this.val = val;
	}

	protected Node getNext() {
		return next;
	}

	protected void setNext(Node next) {
		this.next = next;
	}
	
	
}
