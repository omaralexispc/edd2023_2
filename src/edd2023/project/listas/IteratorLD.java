/**
 * ---------------------------------------------------------------------
 * $Id: IteratorLD.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package edd2023.project.listas;
import java.util.Iterator;
import edd2023.project.listas.DoubleNode;
/**
 * Implementacion de clase para el manejo de Iteradores en una Lista Doble Enlazada<T>.
 * @param <T> Tipo de datos a almacenar en la lista
 * @author Marco Adarme
 * @version 2.0
 */
public class IteratorLD<T extends Comparable> implements Iterator<T>
{
    
    ////////////////////////////////////////////////////////////
    // IteratorLD - Atributos //////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Node de la Lista a Iterar 
     */
    private DoubleNode<T> posicion;   
    
    
    
    
    ////////////////////////////////////////////////////////////
    // IteratorLD - Implementacion de Metodos //////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor con parametros de la clase Iterador de una lista doble <br>
     * <b> post: </b> Se crea un iterador de lista doble. <br>
     * @param posicion es un tipo Node<T> que posee un node de la lista
     */
    IteratorLD(DoubleNode<T> posicion){            
        this.posicion=posicion;	            
    }

    /**
     * Metodo que informa si existe otro elemento en la lista para seguir iterando<br>
     * <b> post: </b> Se retorna si existen aun datos por iterar en la Lista. <br>
     * @return un tipo boolean que informa si existe o no un dato en la lista, desde la posición 
     * actual del cursor.
     */
    @Override
    public boolean hasNext(){            
        return (posicion!=null);            
    }

    /**
     * Metodo que retorna un dato de la posición actual del cursor del iterador.<br>
     * <b> post: </b> Se ha retornado el dato en la posicion actual de la iteracion. <br>
     * El cursor queda en la siguiente posición.
     * @return un tipo T que contiene el dato actual
     */
    @Override
    public T next(){            
        if(!this.hasNext()){                
            System.err.println("Error no hay mas elementos");
        return null;                
        }            
        DoubleNode<T> actual=posicion;
        posicion=posicion.getNext();            
        return(actual.getVal());
    }
    
    /**
     *
     */
    @Override
    public void remove(){}

}//Fin de Clase IteratorLD