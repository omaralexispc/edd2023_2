/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

/**
 *
 * @author Hp
 */
public class ClienteDoble {
    public static void main(String[] args) {
        DoubleLinkedList dl = new DoubleLinkedList();
        dl.add(12);
        dl.add("vamos");
        dl.add(Math.PI);
        dl.print();
        dl.printGetNode(0);
        dl.printGetNode(1);
        dl.printGetNode(2);
    }
   
}
