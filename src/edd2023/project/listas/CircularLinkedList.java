/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

/**
 *
 * @author Hp
 */
public class CircularLinkedList<E extends Comparable> {
    private Node<E> head;
    private Node<E> tail;
    private int size;
    public CircularLinkedList(){
       
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
   
   
    
    public void print(){
        System.out.println("Lista circular:");
        System.out.println(head.getVal());
        Node<E> p = head.getNext();
        while (p!=null&&p!=head){
            System.out.println(p.getVal());
            p = p.getNext();
        }
    }
    
    
    
    public void add(E value){
        if (head==null){
            head = new Node<E> (value);
            size++;
            return;
        }
        if (tail==null){
            tail = new Node<E> (value);
            head.setNext(tail);
            tail.setNext(head);
            size++;
            return;
        }
        
        Node<E> n = new Node(value);
        tail.setNext(n);
        n.setNext(head);
        tail = n;
        size++;
    }
    
    public void addAt(E val,int pos){
        if (pos<0||pos>(size-1)){
            throw new RuntimeException("posicion invalida");
        }
        Node<E> n = new Node(val);
        if (pos==0){
        n.setNext(head);
        head = n;
        tail.setNext(head);
        return;
        }
        
        Node p = getNode(pos-1);
        n.setNext(p.getNext());
        p.setNext(n);
        
        
    }
    
    public E delete (E val){
        E returnable = null;
        if (head.getVal().equals(val) ){
            returnable = head.getVal();
            tail.setNext(head.getNext());
            head = head.getNext();
            //System.out.println("caso head");
            size--;
            
        }
        ///*****Arreglar para el caso tail: potencial desperdicio de memoria
        else{
            //System.out.println("caso normal");
            Node p = head;
            Node q = head.getNext();
            while (q!=null&&q!=head){
                //System.out.println("p:"+p.getVal()+",q:"+q.getVal());
                if (q.getVal().equals(val) ){
                    if (q==tail){
                        //System.out.println("caso tail");
                        tail = p;
                    }
                    p.setNext(q.getNext());
                    size--;
                    returnable = (E) q.getVal();
                }
                p=p.getNext();
                q=q.getNext();
            }
        }
      return returnable; 
    }
    
    public void deleteAt(int pos){
        Node p = getNode(pos-1);
        p.setNext(p.getNext().getNext());
    }
    
    Node getNode (int pos){
        if (pos<0||pos>(size-1)){
            throw new RuntimeException("posicion invalida");
        }
        if (pos==0) return head;
        if (pos==size-1) {
            return tail;
            
        }
        
        Node p = head;
        for (int i = 0; i<pos;i++){
            p= p.getNext();
        }
        
        return p;
        
        
        
    }
    
    
}
