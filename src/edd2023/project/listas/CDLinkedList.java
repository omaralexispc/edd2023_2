/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.listas;

import java.util.Iterator;

/**
 *
 * @author Hp
 */
public class CDLinkedList<E extends Comparable> {
    //sentinel head
    public DoubleNode<E> head;
    public int size;
    
    
    public CDLinkedList(){
        head = new DoubleNode();
        head.setNext(head);
        head.setPrev(head);
    }
    
    public Iterator getIterator(){
        return new IteratorLCD(head);
    }
    
    public void add (E val){
        DoubleNode p = head;
        p = p.getPrev();
        
        DoubleNode<E> n = new DoubleNode<E>(val);
        n.setNext(head);
        head.setPrev(n);
        
        
        n.setPrev(p);
        p.setNext(n);
        size++;
        
    }
    
    public void print(){
        DoubleNode pointer = head.getNext();
		System.out.println("Lista doble circular:");
		while (pointer!=head) {
			System.out.println(pointer.getVal()+" ->");
			pointer = pointer.getNext();
		}
    }
    
     DoubleNode<E> getNode (int index){
        if (size==0) throw new RuntimeException ("Lista vacia");
        if (index<0||index>size-1) throw new RuntimeException ("Indice invalido");
         float mitad = (size-1)/2;
        DoubleNode<E> p;
        if (index<=mitad) {
            //System.out.println("getnode next");
            p = head.getNext();
            for (int i = 0; i < index; i++) {
                p = p.getNext();
            }
        }
        else{
            p = head.getPrev();
            //System.out.println("getnode prev");
            for (int i = size-1; i > index; i--) {
                p = p.getPrev();
            }
        }
        
        
        return p;
    }
     
     public void delete(E val){
         
         DoubleNode pNext = head.getNext();
         DoubleNode pPrev = head.getPrev();
         boolean encontrado = false;
         while (!encontrado){
             pNext = pNext.getNext();
             pPrev = pPrev.getPrev();
             encontrado = pNext.getVal().equals(val) || pPrev.getVal().equals(val);
         }
         
         DoubleNode eliminar;
         if (encontrado){
             if (val.equals(pNext.getVal())) eliminar = pNext;
             else eliminar = pPrev;
             deleteNode(eliminar);
         }
     }
  
  public void deleteAt(int index){
      DoubleNode eliminar = getNode(index);
      deleteNode(eliminar);
  }   
  
  
  private void deleteNode(DoubleNode eliminar){
      eliminar.getPrev().setNext(eliminar.getNext());
      eliminar.getNext().setPrev(eliminar.getPrev());
  }   
}

