/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.arboles;

/**
 *
 * @author estudiante
 */
public class testArbol {
    public static void main(String[] args) {
        ArbolBinario ar = new ArbolBinario();
    NodoBinario n1 = ar.crearArbol(3, null, null);
    NodoBinario n2 = ar.crearArbol(5, null, null);
    NodoBinario n3 = ar.crearArbol(1, n1, n2);
    
    NodoBinario n4 = ar.crearArbol(6, null, null);
    NodoBinario n5 = ar.crearArbol(9, null, null);
    NodoBinario n6 = ar.crearArbol(6, n4, n5);
    
    NodoBinario n7 = ar.crearArbol(0, n3, n6);
    
    ar.imprimirPrefRec(n7);
    }
    
    
    
}
