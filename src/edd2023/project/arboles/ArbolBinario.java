/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.arboles;

/**
 *
 * @author estudiante
 */
public class ArbolBinario<E extends Comparable> {
    NodoBinario<E> raiz;
    
    public ArbolBinario(){
        
    }
    
    public NodoBinario crearArbol(E val, NodoBinario izq, NodoBinario der){
        raiz = new NodoBinario(izq, der, val);
        return raiz;
    }
    
    public NodoBinario getRaiz() {
        return raiz;
    }
    
    
    
    public void imprimirPrefijo(){
        imprimirPrefRec(raiz);
    }
    
    public void imprimirPrefRec(NodoBinario n){
        if (n==null) return;
        else{
        System.out.println(n.getVal());
        imprimirPrefRec(n.getIzquierda());
        imprimirPrefRec(n.getDerecha());
                }
    }
    
    
    
   
}
