/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.arboles;

/**
 *
 * @author estudiante
 */
public class NodoBinario<E extends Comparable> {
    private NodoBinario<E> izquierda;
    private NodoBinario<E> derecha;
    private E val;

    public NodoBinario() {
    }

    public NodoBinario(NodoBinario<E> izquierda, NodoBinario<E> derecha, E val) {
        this.izquierda = izquierda;
        this.derecha = derecha;
        this.val = val;
    }
    
    public boolean isHoja(){
        return izquierda==null&&derecha==null;
    }
    
   

    /**
     * @return the izquierda
     */
    public NodoBinario<E> getIzquierda() {
        return izquierda;
    }

    /**
     * @param izquierda the izquierda to set
     */
    public void setIzquierda(NodoBinario<E> izquierda) {
        this.izquierda = izquierda;
    }

    /**
     * @return the derecha
     */
    public NodoBinario<E> getDerecha() {
        return derecha;
    }

    /**
     * @param derecha the derecha to set
     */
    public void setDerecha(NodoBinario<E> derecha) {
        this.derecha = derecha;
    }

    /**
     * @return the val
     */
    public E getVal() {
        return val;
    }

    /**
     * @param val the val to set
     */
    public void setVal(E val) {
        this.val = val;
    }
    
   
    
}
