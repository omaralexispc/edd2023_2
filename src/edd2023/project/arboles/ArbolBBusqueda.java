/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.project.arboles;

/**
 *
 * @author estudiante
 */
public class ArbolBBusqueda <E extends Comparable> extends ArbolBinario<E> {
    public ArbolBBusqueda(){
        
    }
    
    public void añadirOrdenado (E val){
        if (val==null) return;
        if (raiz==null) raiz = new NodoBinario(null,null,val);
        else{
            añadirRecursivo(raiz,val);
        }
    }
    
    void añadirRecursivo(NodoBinario n, E val){
         if (val.compareTo(n.getVal())<0){
             if (n.getIzquierda()==null) n.setIzquierda(new NodoBinario(null,null,val));
             else añadirRecursivo(n.getIzquierda(),val);
        }
         else if (val.compareTo(n.getVal())>0){
             if (n.getDerecha()==null) n.setDerecha(new NodoBinario(null,null,val));
             else añadirRecursivo(n.getDerecha(),val);
        }
    }
    
    public void eliminarOrdenado(E val){
        NodoBinario aEliminar = buscarNodo(val);
        //System.out.println(aEliminar.getVal());
        //si el nodo no existe
        if (aEliminar==null) return;
        //si el nodo a eliminar es hoja
        
        if (aEliminar.isHoja()) {
                
            eliminarHoja (val,raiz);
        }
        //si el nodo a eliminar tiene un solo hijo
        
        else if (aEliminar.getDerecha()==null||aEliminar.getIzquierda()==null){
            eliminarUnHijo(val,raiz,aEliminar.getIzquierda()!=null);
        }
        
        //si el nodo tiene dos hijos
        
        else{
            
        }
    }
    
    void eliminarDosHijos(E val,NodoBinario n){
        
    }
    
    void eliminarUnHijo(E val, NodoBinario n,boolean izquierdo){
        if (n.getIzquierda().getVal().equals(val)) {
            if (izquierdo){
                n.setIzquierda(n.getIzquierda().getIzquierda());
            }
            else{
                 n.setIzquierda(n.getIzquierda().getDerecha());
            }
        }
        else if (n.getDerecha().getVal().equals(val)) {
           if (izquierdo){
                n.setDerecha(n.getDerecha().getIzquierda());
            }
            else{
                 n.setDerecha(n.getDerecha().getDerecha());
            }
        }
        else{
            if (val.compareTo(n.getVal())<0){
             eliminarUnHijo(val,n.getIzquierda(),izquierdo);
            }
            else {
              eliminarUnHijo(val,n.getIzquierda(),izquierdo);
            }
        }
    }
    void eliminarHoja(E val,NodoBinario n){
        System.out.println(n.getVal());
        if (n.getIzquierda()!=null&&n.getIzquierda().getVal().equals(val)) n.setIzquierda(null);
        else if (n.getDerecha()!=null&&n.getDerecha().getVal().equals(val)) n.setDerecha(null);
        else{
            if (val.compareTo(n.getVal())<0){
             eliminarHoja(val,n.getIzquierda());
            }
            else {
              eliminarHoja(val,n.getDerecha());
            }
        }
        
    }
    
    NodoBinario buscarNodo(E val){
        NodoBinario n = this.raiz;
        return buscarRecursivo(n, val);
    }
    
    NodoBinario buscarRecursivo(NodoBinario n, E val){
        if (n.getVal().equals(val)) return n;
        else{
            if (val.compareTo(n.getVal())<0) return buscarRecursivo(n.getIzquierda(),val);
            else return buscarRecursivo(n.getDerecha(),val);
        }
    }
    
    
    
    public static void main(String[] args) {
        ArbolBBusqueda<Integer> a = new ArbolBBusqueda();
        a.añadirOrdenado(3);
        
        a.añadirOrdenado(2);
        a.añadirOrdenado(1);
        a.añadirOrdenado(5);
        a.añadirOrdenado(4);
        
        a.eliminarOrdenado(5);
        a.imprimirPrefijo();
        //System.out.println("Nodo 2:"+a.buscarNodo(2).getVal());
        
    }
}
