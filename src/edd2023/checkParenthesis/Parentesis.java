/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.checkParenthesis;

import edd2023.project.listas.stack.Stack;

/**
 *
 * @author estudiante
 */
public class Parentesis {
    private Stack<Character> pStack;
    
    public Parentesis(){
        pStack = new Stack();
    }
    
    public boolean isBalanced(String expresion){
        boolean balanced;
        for (char c : expresion.toCharArray()) {
            if (c=='(') pStack.push(c);
            if (c==')'){
                if (pStack.isEmpty()) return false;
                if (pStack.peek()=='(') pStack.pop();
            }
        }
        balanced = pStack.isEmpty();
        return balanced;
    }
}
