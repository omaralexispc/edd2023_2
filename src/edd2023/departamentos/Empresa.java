package edd2023.departamentos;
import edd2023.project.listas.LinkedList;

import java.security.KeyStore.Entry;
import java.util.*;
public class Empresa {
	
	
Map<String,LinkedList> departamentos = new HashMap();

public Empleado buscarCC(int busquedaCC) {
	Empleado e = null;
    for (Map.Entry<String,LinkedList> entry : departamentos.entrySet()) {
			
		LinkedList lista = entry.getValue();
                Iterator it = lista.getIterator();
                while (it.hasNext()){
                    Empleado prov = (Empleado) it.next();
                   if (prov.getCc()==busquedaCC) {
                       e = prov;
                       break;
                   }
                }
                
    }	
	return e;
}

public Empleado buscarApellido(String busquedaApellido) {
	Empleado e = null;
    for (Map.Entry<String,LinkedList> entry : departamentos.entrySet()) {
			
		LinkedList lista = entry.getValue();
                Iterator it = lista.getIterator();
                while (it.hasNext()){
                    Empleado prov = (Empleado) it.next();
                   if (prov.getApellido().equals (busquedaApellido)) {
                       e = prov;
                       break;
                   }
                }
                
    }	
	return e;
}

public Empleado buscarNombre(String busquedaNombre) {
    Empleado e = null;
    for (Map.Entry<String,LinkedList> entry : departamentos.entrySet()) {
			
		LinkedList lista = entry.getValue();
                Iterator it = lista.getIterator();
                while (it.hasNext()){
                    Empleado prov = (Empleado) it.next();
                   if (prov.getNombre().equals (busquedaNombre)) {
                       e = prov;
                       break;
                   }
                }
                
    }	
	return e;
}

public boolean existeDepartamento(String nombre) {
	return departamentos.containsKey(nombre);
}

public boolean moverEmpleado (int cc, String depOrigen,String depDestino) {
    boolean logrado = false;
    boolean existenDep = departamentos.containsKey(depOrigen) && departamentos.containsKey(depDestino);
    if (existenDep){
        Empleado aMover = null;
        LinkedList listaOrigen = departamentos.get(depOrigen);
        Iterator<Empleado> it = listaOrigen.getIterator();
        while (it.hasNext()){
            Empleado e = it.next();
            if (e.getCc()==cc){
                logrado = true;
                aMover = e;
                listaOrigen.delete(e);
                break;
            }
        }
        if (logrado){
            LinkedList listaDestino = departamentos.get(depDestino);
            listaDestino.add(aMover);
        }
    }
    return logrado;
	
}

public boolean fusionarDepartamentos (String depUno,String depDos) {
	//llave primaria sera la de depUno
        boolean fusionados = false;
        LinkedList uno = departamentos.get(depUno);
        LinkedList dos = departamentos.get(depDos);
        fusionados = (uno!=null&&dos!=null);
        if (fusionados) {
            uno.join(dos);
            departamentos.remove(depDos);
                }
	return fusionados;
}

public void mostrarDepartamentos(){
	
	if (departamentos.isEmpty() ) System.out.println("Sin departamentos");
	else {
		for (Map.Entry<String,LinkedList> entry : departamentos.entrySet()) {
			System.out.println("Departamento de "+entry.getKey());
			System.out.println("Empleados:");
			entry.getValue().print();
		}
	}
}

public void crearDepartamento(String nombre) {
	if (existeDepartamento(nombre)) throw new RuntimeException ("Ya existe departamento de "+nombre);
	LinkedList d = new LinkedList<Empleado>();
	departamentos.put(nombre, d);
}

public void ingresarEmpleado (Empleado e, String nombreDepartamento) {
	LinkedList dep = departamentos.get(nombreDepartamento);
	if (dep == null) throw new RuntimeException("No existe departamento"); 
        if(!dep.contains(e) ) dep.add(e);
        else throw new RuntimeException("Ya existe empleado");
         
	
}

}
