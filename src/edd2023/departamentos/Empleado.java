package edd2023.departamentos;

import java.util.Objects;

public class Empleado implements Comparable {
private String nombre,apellido;
private int cc;


public Empleado(String nombre, String apellido, int cc) {
	super();
	this.nombre = nombre;
	this.apellido = apellido;
	this.cc = cc;
}

public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getApellido() {
	return apellido;
}
public void setApellido(String apellido) {
	this.apellido = apellido;
}
public int getCc() {
	return cc;
}
public void setCc(int cc) {
	this.cc = cc;
}

@Override
public int compareTo(Object o) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public String toString() {
	return "Empleado [nombre=" + nombre + ", apellido=" + apellido + ", cc=" + cc + "]";
}

@Override
public int hashCode() {
	return Objects.hash(apellido, cc, nombre);
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Empleado other = (Empleado) obj;
	return  cc == other.cc;
}



}
