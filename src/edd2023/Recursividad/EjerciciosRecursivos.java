/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package edd2023.Recursividad;

/**
 *
 * @author estudiante
 */
public class EjerciciosRecursivos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        //imprimirVocales("palabret");
        //System.out.println(contarCoincidencias("esternocleidomastoideo", 'e'));
        //System.out.println(numBinario (1040101010));
        pruebaCola();
    }
    
    static boolean numBinario (int num){
        return numRecursivo(num,true);
    }
    
    static boolean numRecursivo (int num, boolean cond){
        if (num==0) return true;
        else{
            boolean cond2 = num%10 == 1 || num%10==0;
            return cond && numRecursivo(num/10,cond2);
        }
    }
    
    static void imprimirVocales(String palabra) {
        if (palabra.isBlank()) {
            return;
        }

        imprimirRecursivo(palabra, 0);
    }

    static void imprimirRecursivo(String palabra, int index) {
        if (index == palabra.length()) {
            return;
        } else {
            char c = palabra.charAt(index);
            if (c == 'A' || c == 'a' || c == 'E' || c == 'e' || c == 'I' || c == 'i' || c == 'O' || c == 'o' || c == 'U' || c == 'u') {
                System.out.println(c);
            }
            imprimirRecursivo(palabra, index + 1);
        }
    }

    static int contarCoincidencias(String texto, char letra) {
        if (texto.isEmpty()) {
            return 0;
        }
        return coincidenciasRecursivo(texto, letra, 0);
    }
    
    static int coincidenciasRecursivo (String texto, char letra, int index){
        if (index<texto.length()) return ( letra == texto.charAt(index) )? 1  + coincidenciasRecursivo(texto, letra, index+1) : 
                0  + coincidenciasRecursivo(texto, letra, index+1)  ;
        else return 0;
    }

    static void pruebaCola() {
    Cola cola1 = new Cola();
    Cola cola2 = new Cola();
    
    cola1.encolar(10);
    cola2.encolar(10);
    
    cola1.encolar(10);
    cola2.encolar(10);
    
    cola1.encolar(10);
    cola2.encolar(10);
    
        System.out.println(compararColas(cola2, cola2));
    
    
    }
    
    static boolean compararColas (Cola colaA,Cola colaB){
        if (colaA.isEmpty()&&colaB.isEmpty()){
            
        }
    }

}
