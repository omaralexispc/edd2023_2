/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package edd2023.Recursividad;

/**
 *
 * @author estudiante
 */
public class Recursividad{

     public static void main(String []args){
        //int[] array = {1,2,3,4};
       factorial(4);
     }
     
     static void factorial (int n){
         if (n<0) return;
         System.out.println("factorial "+n+":"+factoRec(n));
     }
     
     static int factoRec(int n)
     {
           if (n==0||n==1) return 1;
           else{
               
             return n*factoRec(n-1);
           }
            
    }
     
     //fibonacci: caso base (0 y 1), resto fibo (n-1)+fibo(n-2)
}